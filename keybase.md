### Keybase proof

I hereby claim:

  * I am iceman1001 on github.
  * I am iceman1001 (https://keybase.io/iceman1001) on keybase.
  * I have a public key ASDyI-k9c1rw6GsFNGbN4lrjRZFWsQYdpz1myx_xv4JxUAo

To claim this, I am signing this object:

```json
{
  "body": {
    "key": {
      "eldest_kid": "0120f223e93d735af0e86b053466cde25ae3459156b1061da73d66cb1ff1bf8271500a",
      "host": "keybase.io",
      "kid": "0120f223e93d735af0e86b053466cde25ae3459156b1061da73d66cb1ff1bf8271500a",
      "uid": "c89faca366db091617fe1bc925471419",
      "username": "iceman1001"
    },
    "merkle_root": {
      "ctime": 1570805373,
      "hash": "72ca4f11abd9cce4f3e92146f87ff471962f6cf0c3e860cc32f6fdbdfee6fc1b2968093c78caf2bfe4c4ae443a382cf0963837ab37df626393192e7c45f1a164",
      "hash_meta": "4105ad0f4970f0dc193e7c9ffafa142c178b6b217c97665e47831e92ec35eb86",
      "seqno": 8011671
    },
    "service": {
      "entropy": "sXOtaC4W6bZ7zBMyWRqVrDBq",
      "name": "github",
      "username": "iceman1001"
    },
    "type": "web_service_binding",
    "version": 2
  },
  "client": {
    "name": "keybase.io go client",
    "version": "4.6.0"
  },
  "ctime": 1570805382,
  "expire_in": 504576000,
  "prev": "a112a5cee879feba95f9deff297d52eb43ee6604c6dfc34e27ad0d4dec2792dd",
  "seqno": 4,
  "tag": "signature"
}
```

with the key [ASDyI-k9c1rw6GsFNGbN4lrjRZFWsQYdpz1myx_xv4JxUAo](https://keybase.io/iceman1001), yielding the signature:

```
hKRib2R5hqhkZXRhY2hlZMOpaGFzaF90eXBlCqNrZXnEIwEg8iPpPXNa8OhrBTRmzeJa40WRVrEGHac9Zssf8b+CcVAKp3BheWxvYWTESpcCBMQgoRKlzuh5/rqV+d7/KX1S60PuZgTG38NOJ60NTewnkt3EIETnQQ9n/RodxgMAneoZGyCjCwBdElFP2wa6FajvbvqvAgHCo3NpZ8RACuqe620oRzLrqugRZVN6rBI5qiSQG9KYcUn1jbnpwL8TKegAenqtBDIQkC873nrRwQGQn7G3YK78+0PoUcwPCqhzaWdfdHlwZSCkaGFzaIKkdHlwZQildmFsdWXEIDhmXkM54wZIZlV6My1p6JoN9oXig05J0w3WvUHQOW94o3RhZ80CAqd2ZXJzaW9uAQ==

```

And finally, I am proving ownership of the github account by posting this as a gist.

### My publicly-auditable identity:

https://keybase.io/iceman1001

### From the command line:

Consider the [keybase command line program](https://keybase.io/download).

```bash
# look me up
keybase id iceman1001
```